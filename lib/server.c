#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <include/server.h>
#include <include/helper.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#define GET_BOOK 'a'
#define PUT_BOOK 'b'
#define BYE_BYE  'z'

int main(int argc, char** argv) {
        char *server_ip = argv[1], *server_port = argv[2];
        if(~(server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))) {
                sockaddr_in server_address = new_address(AF_INET, server_ip, server_port);
                if(~bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address))) {
                        if(~listen(server_socket, SERVER_QUEUE_LENGTH)) {
                                printf("Server successfully started. Waiting for requests in %s:%s\n\n", server_ip, server_port);
                                accept_connection();
                                accept_requests();
                        } else {
                                force_quit("Server error while creating request queue for server socket.");
                        }
                } else {
                        force_quit("Server error while assigning port number to server socket.");
                }
        } else {
                force_quit("Server error while creating server's socket.");
        }
        return 0;
}

void accept_connection(void) {
        int socket_length = sizeof(client_socket);
        sockaddr_in client_address;
        if(~(client_socket = accept(server_socket, (struct sockaddr *) &client_address, &socket_length))) {
                printf("Connection established between client %s and server\n", inet_ntoa(client_address.sin_addr));
        } else {
                force_quit("Server failure while accepting client's incoming connection.");
        }
}

void accept_requests(void) {
        char request[3];
        while(~recv(client_socket, request, sizeof(request), 0), *request != BYE_BYE) {
                process_request(request);
        }
}

void process_request(char* request) {
        char command = *request;
        int  x = request[1], y = request[2];
        switch(command) {
                case GET_BOOK:
                        printf("Buscar livro na posição (%d, %d)\n", x, y);
                        break;
                case PUT_BOOK:
                        printf("Guardar livro na posição (%d, %d)\n", x, y);
                        break;
        }
}
