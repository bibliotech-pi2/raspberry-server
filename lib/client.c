#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <include/client.h>
#include <include/helper.h>

#include <arpa/inet.h>
#include <sys/socket.h>

int main(int argc, char** argv) {
        char *server_ip = argv[1], *server_port = argv[2];
        if(~(client_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))) {
                sockaddr_in server_address = new_address(AF_INET, server_ip, server_port);
                if(~connect(client_socket, (struct sockaddr *) &server_address, sizeof(server_address))) {
                        printf("Successfully connected!\nInsert commands...\n");
                        char option;
                        while(~scanf(" %c", &option)) {
                                printf("Sent %c\n", option);
                                send(client_socket, &option, sizeof(option), 0);
                        }
                } else {
                        force_quit("Client error while establishing connection with server.\n");
                }
        } else {
                force_quit("Client error while creating client's socket.\n");
        }
        return 0;
}
