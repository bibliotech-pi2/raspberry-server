#include <include/helper.h>
#include <stdio.h>
#include <stdlib.h>

sockaddr_in new_address(sa_family_t socket_family, char* server_ip, char* server_port) {
        sockaddr_in address;
        address.sin_family = socket_family;
        address.sin_addr.s_addr = inet_addr(server_ip);
        address.sin_port = htons(atoi(server_port));
        return address;
}

void force_quit(char* message) {
        fprintf(stderr, "%s\n", message);
        exit(EXIT_FAILURE);
}
