#ifndef HELPER_H
#define HELPER_H

#include <arpa/inet.h>

typedef struct sockaddr_in sockaddr_in;

sockaddr_in new_address(sa_family_t socket_family, char* server_ip, char* server_port);

void force_quit(char* message);

#endif
