#ifndef CLIENT_H
#define CLIENT_H

#define SERVER_QUEUE_LENGTH 10

int client_socket;
int server_socket;

void accept_connection(void);
void accept_requests(void);
void process_request(char);

#endif
